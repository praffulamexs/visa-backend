<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    protected $table = "applications";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // public $timestamps = false;

    // Relationships

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{

    protected $table = "people";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // public $timestamps = false;

    // Relationships

}

<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Application;
use App\People;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
// use Mail;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    public function addData(Request $request) {
        
        $owner = $request['owner'];
        $phoneNumber = $request['number'];
        $people = $request['people'];

        $i = 0;

        // $applicationId = substr(md5($phoneNumber), 0, 6);
        
        $applicationIds = Application::pluck('applicationNumber');

        $applicationId = $this->setId($phoneNumber, $i);

        foreach($applicationIds as $application) {

            if ($applicationId == $application) {
                $i = $i+1;
                $applicationId = $this->setId($phoneNumber,$i);
            }

        }

        $total_people = count($people);

        $application = new Application;
        $application->applicationNumber = $applicationId;
        $application->people = $total_people;
        $application->save();
        $format = 'd/m/y';

        foreach($people as $person) {
            $addPerson = new People;

            $addPerson->applicationNumber = $applicationId;
            $addPerson->firstName = $person['personalDetails']['firstName'];
            $addPerson->lastName = $person['personalDetails']['lastName'];
            $addPerson->dob = $person['personalDetails']['dob'];
            $addPerson->countryCitizen = $person['personalDetails']['countryCitizen'];
            $addPerson->gender = $person['personalDetails']['gender'];
            $addPerson->passportNumber = $person['passportDetails']['passportNumber'];
            $addPerson->issueDate = $person['passportDetails']['issueDate'];
            $addPerson->expiryDate = $person['passportDetails']['expiryDate'];
            $addPerson->countryPassport = $person['passportDetails']['countryPassport'];
            $addPerson->travelDate = $person['travelDetails']['travelDate'];
            $addPerson->email = $person['contactDetails']['email'];
            $addPerson->countryCode = $person['contactDetails']['countryCode'];
            $addPerson->phoneNumber = $person['contactDetails']['phoneNumber'];

            $addPerson->save();

        }

        return "Successful";
    }

    function setId($phoneNumber,$i) {

        $applicationNumber = substr(md5($phoneNumber), $i, 6);
        return $applicationNumber;

    }

    function getData() {
        $applications = Application::all();

        foreach($applications as $application) {
            $application['allPeople'] = People::where('applicationNumber',$application['applicationNumber'])->get();
        }

        return $applications;

    }

    function getApplication($applicationNumber)
    {
        return People::where('applicationNumber', $applicationNumber)->get();
    }










    /* MAILS */
    public function basic_email()
    {
        $data = array('name' => "Virat Gandhi");

        Mail::send(['text' => 'mail'], $data, function ($message) {
            $message->to('prafful.amexs@gmail.com', 'Tutorials Point')->subject('Laravel Basic Testing Mail');
            $message->from('prafful.2k17@gmail.com', 'Virat Gandhi');
        });
        echo "Basic Email Sent. Check your inbox.";
    }

    public function html_email()
    {
        $data = array('name' => "Virat Gandhi");
        Mail::send('mail', $data, function ($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject('Laravel HTML Testing Mail');
            $message->from('xyz@gmail.com', 'Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
    }

    public function attachment_email()
    {
        $data = array('name' => "Virat Gandhi");
        Mail::send('mail', $data, function ($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com', 'Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
    /* MAILS */

}